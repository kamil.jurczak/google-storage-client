package pl.jurczak.kamil.google.storage.client;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Google Storage Client
 */
public class GoogleStorageClient {

	private static final String JSON_PATH = "gsc-ws/src/main/google_storage_service_account_key.json";
	private static final String SCOPE_URL_FULL_CONTROL = "https://www.googleapis.com/auth/devstorage.full_control";
	private static final String SCRAPS_STORAGE = "scraps_storage";
	private static final String DOWNLOAD_DIRECTORY = "/home/kamil/Pobrane/scraps/";

	private final Storage storage;

	public GoogleStorageClient() {
		this.storage = buildStorage();
	}

	@SneakyThrows
	private Storage buildStorage() {
		// You can specify a credential file by providing a path to GoogleCredentials.
		// Otherwise credentials are read from the GOOGLE_APPLICATION_CREDENTIALS environment variable.
		final var credentials = GoogleCredentials.fromStream(new FileInputStream(JSON_PATH))
				.createScoped(Lists.newArrayList(SCOPE_URL_FULL_CONTROL));
		return StorageOptions.newBuilder()
				.setCredentials(credentials)
				.build()
				.getService();
	}

	@SneakyThrows
	public File getFile(String fileName, Long fileId) {
		final var blobId = BlobId.of(SCRAPS_STORAGE, fileName, fileId);
		final var blob = storage.get(blobId);
		if (Objects.isNull(blob)) {
			throw new IOException("Cannot find specified file in Google Cloud Storage");
		}
		final var bytes = blob.getContent();
		final var newFile = new File(DOWNLOAD_DIRECTORY + blob.getName());
		FileUtils.writeByteArrayToFile(newFile, bytes);
		return newFile;
	}

	@SneakyThrows
	public Blob sendFile(String path) {
		final var file = new File(path);
		if (!file.exists()) {
			throw new IOException("File not exists");
		}
		final var finalName = file.getName();
		final var blobId = BlobId.of(SCRAPS_STORAGE, finalName);
		final var blobInfo = BlobInfo.newBuilder(blobId).build();
		final var bytes = Files.readAllBytes(Paths.get(path));
		return storage.create(blobInfo, bytes);
	}

	public Page<Bucket> getBuckets() {
		return storage.list();
	}
}
